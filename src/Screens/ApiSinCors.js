import React, { useEffect, useState } from 'react';
import '../App.css';

const ApiSinCors = () => {
	const [data, setData] = useState([]);
	const [error, setError] = useState(null);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await fetch(
					'https://cat-fact.herokuapp.com/facts'
				);

				if (!response.ok) {
					throw new Error('Error en la solicitud a la API');
				}

				const jsonData = await response.json();
				setData(jsonData);
			} catch (error) {
				setError(error.message);
			}
		};

		fetchData();
	}, []);

	return (
		<div className='App'>
			<header className='App-header'>
				<h1>Datos raros de gatos</h1>
				<div style={containerStyle}>
					{data.length > 0 ? (
						data.map((cat) => (
							<div key={cat.id} style={cardStyle}>
								<h2>{cat.text}</h2>
								<p>Creación: {cat.createdAt}</p>
							</div>
						))
					) : error ? (
						<p>Error: {error}</p>
					) : (
						<p>Cargando datos...</p>
					)}
				</div>
			</header>
		</div>
	);
};

export default ApiSinCors;

const cardStyle = {
	border: '1px solid #ccc',
	borderRadius: '5px',
	padding: '10px',
	margin: '10px',
	backgroundColor: '#40376E',
};

const containerStyle = {
	maxWidth: '80%',
	margin: '0 auto',
};
