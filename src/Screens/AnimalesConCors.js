import React, { useEffect, useState } from 'react';

const AnimalesConCors = () => {
	const [data, setData] = useState(null);
	const [error, setError] = useState(null);

	useEffect(() => {
		const fetchData = async () => {
			try {
				const response = await fetch(
					'https://api.petfinder.com/v2/animals'
				);

				if (!response.ok) {
					throw new Error('Error en la solicitud a la API');
				}

				const jsonData = await response.json();
				setData(jsonData);
			} catch (error) {
				setError(error.message);
			}
		};

		fetchData();
	}, []);

	return (
		<div>
			{error ? (
				<p>Error: {error}</p>
			) : (
				<pre>{JSON.stringify(data, null, 2)}</pre>
			)}
		</div>
	);
};

export default AnimalesConCors;
