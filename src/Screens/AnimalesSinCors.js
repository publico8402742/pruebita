import React, { useEffect, useState } from 'react';
import '../App.css';
import { getAnimals } from '../api/apiAnimals';
//aber

export default function AnimalesSinCors() {
	const [animalsData, setAnimalsData] = useState([]);

	useEffect(() => {
		// Llama a la función getAnimals y actualiza el estado con los datos obtenidos
		getAnimals()
			.then((data) => {
				setAnimalsData(data.animals);
			})
			.catch((error) => {
				console.error('Error al obtener los animales:', error);
			});
	}, []);

	return (
		<div className='App'>
			<header className='App-header'>
				<h1>Listona de Animales</h1>
				<div style={containerStyle}>
					{animalsData.map((animal) => (
						<div key={animal.id} style={cardStyle}>
							<h2>{animal.name}</h2>
							<p>Edad: {animal.age}</p>
							<p>Descripción: {animal.description}</p>
						</div>
					))}
				</div>
			</header>
		</div>
	);
}

const cardStyle = {
	border: '1px solid #ccc',
	borderRadius: '5px',
	padding: '10px',
	margin: '10px',
	backgroundColor: '#40376E',
};
const containerStyle = {
	maxWidth: '80%',
	margin: '0 auto',
};
