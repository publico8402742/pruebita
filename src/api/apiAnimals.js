// apiAnimals.js

const clientId = process.env.REACT_APP_CLIENT_ID;
const clientSecret = process.env.REACT_APP_CLIENT_SECRET;

const apiUrl = 'https://api.petfinder.com/v2/oauth2/token';
const apiUrlAnimals = 'https://api.petfinder.com/v2/animals';

export async function getToken() {
	const requestData = new URLSearchParams();
	requestData.append('grant_type', 'client_credentials');
	requestData.append('client_id', clientId);
	requestData.append('client_secret', clientSecret);

	try {
		const response = await fetch(apiUrl, {
			method: 'POST',
			body: requestData,
		});

		if (!response.ok) {
			throw new Error('Error al obtener el token');
		}

		const data = await response.json();
		return data.access_token;
	} catch (error) {
		console.error('Error al obtener el token:', error);
		throw error;
	}
}

export async function getAnimals() {
	try {
		const accessToken = await getToken();

		const response = await fetch(apiUrlAnimals, {
			method: 'GET',
			headers: {
				Authorization: `Bearer ${accessToken}`,
			},
		});

		if (!response.ok) {
			throw new Error('Error al obtener los animales');
		}

		const data = await response.json();
		return data; // En lugar de imprimir, devuelve los datos
	} catch (error) {
		console.error('Error al obtener los animales:', error);
		throw error;
	}
}
