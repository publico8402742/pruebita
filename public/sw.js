// self.addEventListener('fetch', (event) => {
// 	const oflineResponse = new Response(`Bienvenido a mi pagina web
// 	Disculpa , pero necesitas internet para usarla`);
// 	const response = fetch(event.request).catch(() => oflineResponse);
// 	event.respondWith(response);
// });
// self.addEventListener('fetch', (event) => {
// 	const oflineResponse = new Response(
// 		`<!DOCTYPE html>
// 	 <html lang="en">
// 		 <head>
// 			<title>React app</title>
// 		 </head>
// 		 <body>
// 		<h1>Ofline Mode</h1>
// 		 </body>
// 	 </html>`,
// 		{
// 			headers: {
// 				'Content-Type': 'text/html',
// 			},
// 		}
// 	);
// 	const response = fetch(event.request).catch(() => oflineResponse);
// 	event.respondWith(response);
// });

// self.addEventListener('install', function (event) {
// 	const cahePromise = caches.open('cache.2').then(function (cache) {
// 		return cache.addAll([
// 			'/',
// 			'/index.html',
// 			'/js/app.js',
// 			'/sw.js',
// 			'static/js/bundle.js',
// 			'favicon.ico',
// 		]);
// 	});
// 	event.waitUntil(cahePromise);
// });

// self.addEventListener('fetch', function (event) {
// 	event.respondWith(caches.match(event.request));
// });
// self.addEventListener('fetch', function (event) {
// 	const respuesta = caches.match(event.request).then((response) => {
// 		if (response) return response;
// 		return fetch(event.request).then((newResponse) => {
// 			//agregar al cache .clon
// 			caches.open('cache.2').then((cache) => {
// 				cache.put(event.request, newResponse);
// 			});
// 			return newResponse.clone();
// 		});
// 	});
// 	event.respondWith(respuesta);
// });
const CACHE_DYNAMIC = 'dynamic-v1'; //para los archivos que se van a descargar
const CACHE_STATIC = 'static-v1'; //app shell carga archivos necesarios para cargar la app
const CACHE_INMUTABLE = 'inmutable-v1'; //CDN de terceros . LIBRERIAS //peticiones externas

const limpiarCache = (cacheName, numberItem) => {
	caches.open(cacheName).then((cache) => {
		cache.keys().then((keys) => {
			if (keys.length > numberItem) {
				cache.delete(keys[0]).then(limpiarCache(cacheName, numberItem));
			}
		});
	});
};

self.addEventListener('install', function (event) {
	const cahePromise = caches.open(CACHE_STATIC).then(function (cache) {
		return cache.addAll([
			'/',
			'/index.html',
			'/js/app.js',
			'/sw.js',
			'static/js/bundle.js',
			'favicon.ico',
			'not-found.jpg',
			'pages/offline.html',
		]);
	});
	const caheInmutable = caches.open(CACHE_INMUTABLE).then(function (cache) {
		return cache.addAll([
			'https://fonts.googleapis.com/css2?family=Roboto:ital@1&display=swap',
		]);
	});
	event.waitUntil(Promise.all([cahePromise, caheInmutable]));
});

// self.addEventListener('fetch', function (event) {
// 	const respuesta = caches.match(event.request).then((response) => {
// 		if (response) return response;
// 		return fetch(event.request).then((newResponse) => {
// 			//agregar al cache .clon
// 			caches.open(CACHE_DYNAMIC).then((cache) => {
// 				cache.put(event.request, newResponse);
// 				limpiarCache(CACHE_DYNAMIC, 20);
// 			});
// 			return newResponse.clone();
// 		});
// 	});
// 	event.respondWith(respuesta);
// });
//invertir el cache para traer archivos de internet primero antes que del cache
// self.addEventListener('fetch', function (event) {
// 	// Intentamos buscar una respuesta en la caché dinámica (CACHE_DYNAMIC)
// 	const response = caches.open(CACHE_DYNAMIC).then((cache) => {
// 		// Intentamos obtener una respuesta en la caché para la solicitud actual
// 		return cache.match(event.request).then((response) => {
// 			// Creamos una promesa para realizar una solicitud de red (fetch)
// 			const fetchPromise = fetch(event.request).then((networkResponse) => {
// 				// Almacenamos la respuesta de la red en la caché dinámica
// 				cache.put(event.request, networkResponse.clone());
// 				// Llamamos a la función para limpiar la caché (mantener 20 elementos)
// 				limpiarCache(CACHE_DYNAMIC, 2);
// 				// Devolvemos la respuesta de la red
// 				return networkResponse;
// 			});

// 			// Devolvemos la respuesta de la caché si está disponible, de lo contrario, la respuesta de la red
// 			return response || fetchPromise;
// 		});
// 	});

// 	// Respondemos con la respuesta seleccionada (caché o red)
// 	event.respondWith(response);
// });

// self.addEventListener('fetch', function (event) {
// 	// Intentamos buscar una respuesta en la caché dinámica (CACHE_DYNAMIC)
// 	const response = fetch(event.request)
// 		.then((res) => {
// 			caches.open(CACHE_DYNAMIC).then((cache) => {
// 				cache.put(event.request, res);
// 				limpiarCache(CACHE_DYNAMIC, 10);
// 			});
// 			return res.clone();
// 		})
// 		.catch((err) => {
// 			return caches.match(event.request);
// 		});
// 	event.respondWith(response);
// });

// Cache Network race
// self.addEventListener('fetch', function (event) {
// 	const respuesta = new Promise((resolve, reject) => {
// 		let peticionRechazada = false;
// 		const falloSolicitud = () => {
// 			if (peticionRechazada) {
// 				if (/\.(png|jpeg)$/i.test(event.request.url)) {
// 					resolve(caches.match('/not-found.jpg'));
// 				} else {
// 					reject('No se encontro respuesta');
// 				}
// 			} else {
// 				peticionRechazada = true;
// 			}
// 		};

// 		fetch(event.request)
// 			.then((res) => {
// 				res.ok ? resolve(res) : falloSolicitud();
// 			})
// 			.catch(falloSolicitud);
// 		//Luego busca en el cache
// 		caches
// 			.match(event.request)
// 			.then((res) => {
// 				res ? resolve(res) : falloSolicitud();
// 			})
// 			.catch(falloSolicitud);
// 	});
// 	event.respondWith(respuesta);
// });

self.addEventListener('fetch', function (event) {
	//Cache with network fallback
	const respuesta = caches.match(event.request).then((response) => {
		if (response) return response;
		//Si no existe el archivo lo descarga de la web
		return fetch(event.request)
			.then((newResponse) => {
				caches.open(CACHE_DYNAMIC).then((cache) => {
					cache.put(event.request, newResponse);
					limpiarCache(CACHE_DYNAMIC, 20);
				});
				return newResponse.clone();
			}) //ToDo 2 Manejo de errores
			.catch((err) => {
				if (event.request.headers.get('accept').includes('text/html')) {
					return caches.match('pages/offline.html');
				}
			});
	});
	event.respondWith(respuesta);
});
